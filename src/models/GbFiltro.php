<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use LocknLoad\Crud\ModelCore;

class GbFiltro extends ModelCore
{

    use SoftDeletes;

    protected $table = 'gb_filtros';
    protected $softDelete = true;
    protected static $manyToMany;

    public function __construct(){
        static::$manyToMany = array('ec_categoria','ec_produto');
    }

    public function produto() {
        return $this->belongsToMany('App\EcProduto', 'ec_produtos_has_gb_filtros', 'gb_filtros_id', 'ec_produtos_id')->withPivot('valor');
    }

    public function categoria() {
        return $this->belongsToMany('App\EcCategoria', 'ec_categorias_has_gb_filtros',  'gb_filtros_id', 'ec_categorias_id');
    }

    public function presentation(){
        return $this->label;
    }
}
