<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use LocknLoad\Crud\ModelCore;

class ScMenu extends ModelCore
{

    use SoftDeletes;

    protected $table = 'sc_menus';
    protected $softDelete = true;
    protected static $manyToMany;

    public static function getSubMenu($id){
        return ScMenu::where('id_sc_menu',$id)->get();
    }

    public static function getRoot(){
        return ScMenu::where('id_sc_menu',0)->get();
    }


    public function presentation(){
        return $this->label;
    }
}
