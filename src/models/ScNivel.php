<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use LocknLoad\Crud\ModelCore;

class ScNivel extends ModelCore
{

    use SoftDeletes;

    protected $table = 'sc_niveis';
    protected $softDelete = true;
    protected static $manyToMany;

    public function presentation(){
        return $this->nome;
    }
}
