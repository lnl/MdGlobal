<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateScMenusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sc_menus', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_sc_menu')->unsigned()->nullable();
			$table->string('label', 100);
			$table->string('model', 200)->nullable();
			$table->enum('type', array('header','dropdown','submenu','item'))->default('item');
			$table->integer('access_level')->default(1);
			$table->enum('icon', array('nenhum','fa-file','fa-file-image-o','fa-user','fa-cog'))->default('nenhum');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sc_menus');
	}

}
