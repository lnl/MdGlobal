<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGbImagensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gb_imagens', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nome', 100)->nullable();
			$table->string('imagem', 200);
			$table->string('imagem_mobile', 200)->nullable();
			$table->string('alt', 100)->nullable();
			$table->string('title', 100)->nullable();
			$table->text('descricao', 65535)->nullable();
			$table->string('credito', 100)->nullable();
			$table->integer('tamanho')->unsigned()->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gb_imagens');
	}

}
