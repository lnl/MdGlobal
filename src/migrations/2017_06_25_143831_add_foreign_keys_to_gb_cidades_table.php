<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToGbCidadesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('gb_cidades', function(Blueprint $table)
		{
			$table->foreign('id_gb_estado')->references('id')->on('gb_estados')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('gb_cidades', function(Blueprint $table)
		{
			$table->dropForeign('gb_cidades_id_gb_estado_foreign');
		});
	}

}
