<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGbImagensHasEcProdutosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gb_imagens_has_ec_produtos', function(Blueprint $table)
		{
			$table->integer('gb_imagens_id')->unsigned()->index('gb_imagens_has_ec_produtos_gb_imagens_id_foreign');
			$table->integer('ec_produtos_id')->unsigned()->index('gb_imagens_has_ec_produtos_ec_produtos_id_foreign');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gb_imagens_has_ec_produtos');
	}

}
